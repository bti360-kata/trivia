Legacy Code Retreat code base
======

Use this code base to run your own [Legacy Code Retreat](http://legacycoderetreat.jbrains.ca).

As of this writing, there isn't really a single place to get all the information you might want about Legacy Code Retreat. Search the web and ask your colleagues. Most importantly, don't panic! If you've been to Code Retreat even once, then you know most of what you need to run a Legacy Code Retreat. Give it a try!

## IDE Setup
### Eclipse
You can either use the Gradle Eclispe Integration workshop plugin, which allows importing a gradle project and keeping the project files synced
with the gradle dependencies and build file, or manually generate the eclispe project files using `./gradlew eclipse`

### IntelliJ
You should be able to just import the project as a gradle project using the default gradle wrapper.

