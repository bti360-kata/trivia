import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.util.Random;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

import com.adaptionsoft.games.trivia.runner.GameRunner;

public class CharacterizationTest {

	@Test
	public void test() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		ByteArrayOutputStream baos = null;
		String output = "";
		try {
			baos = new ByteArrayOutputStream();
			System.setOut(new PrintStream(baos));
			Random rand = new Random();
		
			for (int seed = 0; seed < 1000; seed++){
				rand.setSeed(seed);
				GameRunner.run(rand);
			}
		} finally { 
			System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
			output = baos.toString();
		}
		String shah = DigestUtils.shaHex(output.getBytes());
		assertEquals("30739107b7788c2c5353dde54fe1d851db058b6b", shah);
	}

}
